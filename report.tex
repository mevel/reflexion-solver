\documentclass[a4paper]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
%\usepackage[francais]{babel}
%\frenchbsetup{og=«,fg=»}
\DeclareUnicodeCharacter{A0}{~}

\newcommand\bs{\textbackslash}
\newcommand\du{\uparrow}
\newcommand\dd{\downarrow}
\newcommand\dl{\leftarrow}
\newcommand\dr{\rightarrow}
\newcommand\dest{\mathrm{dest}}

\usepackage{graphicx} % pour inclure des images (commande \includegraphics)
\usepackage{wrapfig}  % environnement ‘wrapfigure’
\usepackage{tikz}     % pour dessiner des graphes
\usepackage{amsmath}  % commandes mathématiques (dont l’environnement ‘align’)

% générer un sommaire pour le document PDF, et activer les liens cliquables :
\usepackage{hyperref}
% changer le style des liens (encadrés en rouge par défaut) :
\hypersetup{pdftex,colorlinks=true,allcolors=blue}

\usepackage[vmargin=3cm, hmargin=3.7cm]{geometry}

% en-tête :
\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhead[L]{March 6, 2015}
\fancyhead[C]{Projet logique, part. 1}
\fancyhead[R]{Glen \textsc{Mével}}

% métadonnées :
\title{Programmation logique — Projet, part. 1}
\author{Glen \textsc{Mével}}
\date{March 6, 2015}

\begin{document}


\maketitle

\begin{abstract}
	The source code is available at
	\url{https://github.com/gmevel/reflexion-solver}.
	Useful files are \texttt{reflexion-solver.ml} (the main program) and
	\texttt{reflexion.mp} (the MetaPost package for producing PDF images).
	Instruction on how to run the program are to be found in the comment at the
	beginning of the source code. It requires a (recent) installation of OCaml,
	the programs \texttt{z3} and \texttt{mptopdf} (for PDF generation).
\end{abstract}

\section{First part: using pure SAT}

\subsection{Building the CNF formula}

\paragraph{}
We first notice than freezing the mirrors once for all leads to simplifications
of the problem. A ray emitted from the source and reaching the exit cannot be
reflected by walls; otherwise, it will follow the path exactly opposite to its
previous path and reach the source again.

\paragraph{}
We have adopted the following approach to translate a Reflexion board into a
logical formula: of course we need one boolean variable per mirror. The boolean
variable~$m_i$ will be set to true if the mirror is oriented as ‘\texttt{/}’ and
false if it is oriented as ‘\texttt{\bs}’.

\paragraph{}
We then need to encode the ray paths. Apart from the source and exit, those rays
move from one mirror to another (since we not want them to reflect on walls),
thus it suffices to consider the mirrors.  We create eight variables per
mirror~$\texttt M_i$: $a_{i,d}$ and $r_{i,d}$ where $d \in \{\du;\dd;\dl;\dr\}$.
The variable~$a_{i,d}$ means that a ray with direction $d$ reaches the mirror;
the variable~$r_{i,d}$ means that a ray is emitted from the mirror towards
direction~$d$. We then define the following formulas to traduce the reflections:
\begin{align*}
	a_{i,\du} \land m_i &\implies r_{i,\dr}        \tag{refl$_{/,\du}$} \\
	a_{i,\dd} \land m_i &\implies r_{i,\dl}        \tag{refl$_{/,\dd}$} \\
	a_{i,\dl} \land m_i &\implies r_{i,\dd}        \tag{refl$_{/,\dl}$} \\
	a_{i,\dr} \land m_i &\implies r_{i,\du}        \tag{refl$_{/,\dr}$} \\
	a_{i,\du} \land \lnot m_i &\implies r_{i,\dl}  \tag{refl$_{\bs,\du}$} \\
	a_{i,\dd} \land \lnot m_i &\implies r_{i,\dr}  \tag{refl$_{\bs,\dd}$} \\
	a_{i,\dl} \land \lnot m_i &\implies r_{i,\du}  \tag{refl$_{\bs,\dl}$} \\
	a_{i,\dr} \land \lnot m_i &\implies r_{i,\dd}  \tag{refl$_{\bs,\dr}$}
\end{align*}

The translation to disjunctive clauses is straightforward:
$$\ell_1 \land … \land \ell_n \implies c
\quad\equiv\quad \lnot\ell_1 \lor … \lor \lnot\ell_n \lor c$$

\paragraph{}
Then, we must describe the progress of the ray.  For a cell~$c$ (its
coordinates) and a direction~$d$, we define:
$$\dest(c,d) = \begin{cases}
	a_{i,d} &\text{if starting from cell~$c$ with direction~$d$, we reach the
	               mirror~$i$} \\
	\top    &\text{else if we reach the exit \texttt E} \\
	\bot    &\text{otherwise (we reach the source \texttt S or a wall)}
\end{cases}$$

For each mirror~$\texttt M_i$, we add the following four clauses to traduce what
rays become after reflection on the mirror:
\begin{equation}
	r_{i,d} \implies \dest(\texttt M_i,d)
	\qquad\text{for every $d \in \{\du;\dd;\dl;\dr\}$}  \label{dest}\tag{dest$_d$}
\end{equation}

Indeed, if a ray is emitted from mirror~$i$ ($r_{i,d}$ is true), then:
\begin{itemize}
	\item either it reaches another mirror~$i'$ ($a_{i',d}$ is true),
	\item or it reaches the exit, what adds no constraint (which is effectively
		the meaning of $r_{i,d} \implies \top$),
	\item or it reaches the source or a wall, what is excluded ($r_{i,d}.
		\implies \bot$)
\end{itemize}

\paragraph{}
We traduce the fact that a ray is emitted from the source in a similar way:
\begin{equation}
	\dest(\texttt S, d_{\texttt S})  \label{source}\tag{source}
\end{equation}

\paragraph{}
It only remains to specify that a ray must visit each diamond cell. To
achieve this, we add one clause per diamond~$\texttt D_j$:
\begin{equation}
	\dest(\texttt D_j, \du)
	\lor \dest(\texttt D_j, \dd)
	\lor \dest(\texttt D_j, \dl)
	\lor \dest(\texttt D_j, \dr)  \tag{diam}
\end{equation}

For $M$ variables and $D$ diamonds, we produce $9M$ variables and $12M+D+1$
clauses, each of them including at most four literal.

\subsection{Proof}

\paragraph{}
A valuation of such a set of variables defines a board with each mirror having
an orientation (variables $m_i$), and (possibly several) rays that runs through
this board (variables $a_{i,d}$ and $r_{i,d}$). We can show that this set of
clauses suffices to characterize \textit{pseudo-solutions}. We call
pseudo-solution a configuration in which each diamond is visited by at least one
ray, and in which there is a ray emitted from the source and reaching the exit.

\paragraph{}
We can show that such a configuration can only contain the ray emitted from
\texttt S and possibly others which are cyclic. Indeed, as shown by figure
\ref{graph}, we can see the board as a non-oriented graph: each mirror is
equivalent to two nodes that each connect with two neighbours. The source, exit
and walls are nodes connected to at most one node, the one from which they can
be reached.

In this graph, each node has at most two edges, so connected components are
paths that can either run from one end to another or be cyclic. The constraints
(\ref{dest}) ensure that a ray emitted from some node always continues running
as long as it can, and that it cannot reach a wall nor the source. So, such a
ray can only be a cyclic connected component, or reach the exit. In the last
case, the ray is in fact the one emitted from the source, which must exist by
constraint (\ref{source}). Indeed, the ray emitted from the source cannot be
cyclic and its only possible end point is the exit.

\paragraph{}
Because of these cycles, it is not sufficient to fully solve the Reflexion game
using SAT. In our program we use incremental solving: we pass to the \texttt{z3}
SAT-solver the CNF defined above, then if it returns a valuation, we check that
it is not a false positive (every diamond is reached from the source). If
successful, we have a real solution, otherwise we add a clause that prevent this
valuation and solve the formula again.

\begin{figure}[p]
	\centering
	\caption{A game board seen as a graph}
	\label{graph}

	\textit{This board:}

	\includegraphics[scale=2.75]{report-graph-example}

	\textit{can be regarded as a graph (each connected component has a different
	colour):}

	\begin{tikzpicture}[scale=1.5]
		\def \w {.7} % nodes width (in cm)
		\def \e {10} % spacing between two halves of a mirror (in degrees)
		\tikzstyle {edge} = [very thick]
		\tikzstyle {final} = [draw, rectangle, minimum size=\w cm, fill=gray!60]
		\tikzstyle {mirror} = [fill=blue!70]
		\newcommand \mirrorP[2]{
			\begin{scope}[shift={#2}]
				\node[rectangle, minimum size=\w cm] (#1a) at (0,0) {};
				\node[rectangle, minimum size=\w cm] (#1b) at (0,0) {};
				\filldraw[mirror] (#1a.45+\e) -- (#1a.north west) -- (#1a.225-\e) -- cycle;
				\filldraw[mirror] (#1a.45-\e) -- (#1a.south east) -- (#1a.225+\e) -- cycle;
			\end{scope}
		}
		\newcommand \mirrorM[2]{
			\begin{scope}[shift={#2}]
				\node[rectangle, minimum size=\w cm] (#1a) at (0,0) {};
				\node[rectangle, minimum size=\w cm] (#1b) at (0,0) {};
				\filldraw[mirror] (#1a.-45+\e) -- (#1a.north east) -- (#1a.135-\e) -- cycle;
				\filldraw[mirror] (#1a.-45-\e) -- (#1a.south west) -- (#1a.135+\e) -- cycle;
			\end{scope}
		}
		\node[final, fill=green!80] (S) at (0,3) {\texttt S};
		\node[final, fill=red!60]   (E) at (6,2) {\texttt E};
		\mirrorP {M1} {(2,4)};
		\mirrorM {M2} {(4,4)};
		\mirrorM {M3} {(2,3)};
		\mirrorP {M4} {(4,3)};
		\mirrorM {M5} {(2,2)};
		\node[final] (W1) at (2,6) {};
		\node[final] (W2) at (4,6) {};
		\node[final] (W3) at (0,4) {};
		\node[final] (W4) at (6,4) {};
		\node[final] (W5) at (6,3) {};
		\node[final] (W6) at (0,2) {};
		\node[final] (W7) at (2,0) {};
		\node[final] (W8) at (4,0) {};
		\draw[edge,green!50!black] (W3) -- (M1a) -- (W1);
		\draw[edge,brown]          (W2) -- (M2a) -- (W4);
		\draw[edge,teal]           (W6) -- (M5b) -- (W7);
		\draw[edge,orange]         (W8) -- (M4b) -- (W5);
		\draw[edge,magenta]        (M1b) -- (M2b) -- (M4a) -- (M3a) -- (M1b);
		\draw[line width=4pt, white] (M5a) -- (E);
		\draw[edge,blue]           (S) -- (M3b) -- (M5a) -- (E);
	\end{tikzpicture}
\end{figure}

\section{Second part: using SMT}

\paragraph{}
Using SMT, we can overcome this problem directly in the logic. We can forbid
circular rays by defining a quantity that must increase between each node
visited in the graph. To that purpose we replace the boolean variables $a_{i,d}$
and $r_{i,d}$ with integer variables $a'_{i,d}$ and $r'_{i,d}$.

Instead of the booleans $\top$ and $\bot$, we also introduce integer variables
$e$ and $s$ which will serve as reference values: $s$ is meant to be assigned to
the source and $e$ to the exit. Literals like $a_{i,d}$ become $(s < a'_{i,d})$,
literals like $\lnot a_{i,d}$ become $(a'_{i,d} < s)$, and literals like
$r_{i,d} \implies \top$ will conveniently be written $(r'_{i,d} < e)$ because
$e$ will be allowed to be arbitrary great so such a literal will always be true.

\paragraph{}
We can then translate the previous set of SAT clauses into similarly shaped SMT
clauses:

\begin{align*}
	m_i &\implies a'_{i,\du} = r'_{i,\dr}        \tag{refl'$_{/,\du}$} \\
	m_i &\implies a'_{i,\dd} = r'_{i,\dl}        \tag{refl'$_{/,\dd}$} \\
	m_i &\implies a'_{i,\dl} = r'_{i,\du}        \tag{refl'$_{/,\dl}$} \\
	m_i &\implies a'_{i,\dr} = r'_{i,\dd}        \tag{refl'$_{/,\dr}$} \\
	\lnot m_i &\implies a'_{i,\du} = r'_{i,\dl}  \tag{refl'$_{\bs,\du}$} \\
	\lnot m_i &\implies a'_{i,\dd} = r'_{i,\dr}  \tag{refl'$_{\bs,\dd}$} \\
	\lnot m_i &\implies a'_{i,\dl} = r'_{i,\dd}  \tag{refl'$_{\bs,\dl}$} \\
	\lnot m_i &\implies a'_{i,\dr} = r'_{i,\du}  \tag{refl'$_{\bs,\dr}$}
\end{align*}
\begin{equation}
	s < r_{i,d} \implies r_{i,d} < \dest'(\texttt M_i,d)
	\qquad\text{for every $d \in \{\du;\dd;\dl;\dr\}$}  \label{dest'}\tag{dest'$_d$}
\end{equation}
\begin{equation}
	s < \dest'(\texttt S, d_{\texttt S})  \tag{source'}
\end{equation}
\begin{equation}
	s < \dest(\texttt D_j, \du)
	\lor s < \dest'(\texttt D_j, \dd)
	\lor s < \dest'(\texttt D_j, \dl)
	\lor s < \dest'(\texttt D_j, \dr)  \tag{diam'}
\end{equation}
where we define again:
$$\dest'(c,d) = \begin{cases}
	a'_{i,d} &\text{if starting from cell~$c$ with direction~$d$, we reach the
	                mirror~$i$} \\
	e        &\text{else if we reach the exit \texttt E} \\
	s        &\text{otherwise (we reach the source \texttt S or a wall)}
\end{cases}$$

\paragraph{}
This set of clauses keeps the characteristics of the previous set, and forbids
cycles because of the new form of clause (\ref{dest'}). By consequence, only
true solutions of the problem satisfies that formula.


\begin{figure}[b]
	\centering
	\includegraphics[width=0.4\textwidth]{/home/glen/Documents/Pictures/Cygne}
\end{figure}

\end{document}
