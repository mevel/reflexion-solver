%!PS
%%BoundingBox: -9 -103 103 9 
%%HiResBoundingBox: -8.12402 -102.61221 102.61221 8.12402 
%%Creator: MetaPost 1.902
%%CreationDate: 2015.03.07:1956
%%Pages: 1
%*Font: cmmi10 14.94397 9.96265 45:8002
%%BeginProlog
%%EndProlog
%%Page: 1 1
 0 0.7 0 setrgbcolor 0 0.5 dtransform truncate idtransform setlinewidth pop
 [3 3 ] 0 setdash 1 setlinecap 1 setlinejoin 10 setmiterlimit
newpath 0 -47.2441 moveto
0 -47.2441 lineto
31.49606 -47.2441 lineto
31.49606 -62.99213 lineto
94.48819 -62.99213 lineto stroke
 0.3 0.3 0.3 setrgbcolor
newpath -7.87402 -7.87402 moveto
7.87402 -7.87402 lineto
7.87402 7.87402 lineto
-7.87402 7.87402 lineto
 closepath fill
newpath 7.87402 -7.87402 moveto
23.62206 -7.87402 lineto
23.62206 7.87402 lineto
7.87402 7.87402 lineto
 closepath fill
newpath 23.62206 -7.87402 moveto
39.37009 -7.87402 lineto
39.37009 7.87402 lineto
23.62206 7.87402 lineto
 closepath fill
newpath 39.37009 -7.87402 moveto
55.11812 -7.87402 lineto
55.11812 7.87402 lineto
39.37009 7.87402 lineto
 closepath fill
newpath 55.11812 -7.87402 moveto
70.86615 -7.87402 lineto
70.86615 7.87402 lineto
55.11812 7.87402 lineto
 closepath fill
newpath 70.86615 -7.87402 moveto
86.61418 -7.87402 lineto
86.61418 7.87402 lineto
70.86615 7.87402 lineto
 closepath fill
newpath 86.61418 -7.87402 moveto
102.36221 -7.87402 lineto
102.36221 7.87402 lineto
86.61418 7.87402 lineto
 closepath fill
newpath -7.87402 -23.62206 moveto
7.87402 -23.62206 lineto
7.87402 -7.87402 lineto
-7.87402 -7.87402 lineto
 closepath fill
newpath 86.61418 -23.62206 moveto
102.36221 -23.62206 lineto
102.36221 -7.87402 lineto
86.61418 -7.87402 lineto
 closepath fill
newpath -7.87402 -39.37009 moveto
7.87402 -39.37009 lineto
7.87402 -23.62206 lineto
-7.87402 -23.62206 lineto
 closepath fill
newpath 86.61418 -39.37009 moveto
102.36221 -39.37009 lineto
102.36221 -23.62206 lineto
86.61418 -23.62206 lineto
 closepath fill
newpath 86.61418 -55.11812 moveto
102.36221 -55.11812 lineto
102.36221 -39.37009 lineto
86.61418 -39.37009 lineto
 closepath fill
newpath -7.87402 -70.86615 moveto
7.87402 -70.86615 lineto
7.87402 -55.11812 lineto
-7.87402 -55.11812 lineto
 closepath fill
newpath -7.87402 -86.61418 moveto
7.87402 -86.61418 lineto
7.87402 -70.86615 lineto
-7.87402 -70.86615 lineto
 closepath fill
newpath 86.61418 -86.61418 moveto
102.36221 -86.61418 lineto
102.36221 -70.86615 lineto
86.61418 -70.86615 lineto
 closepath fill
newpath -7.87402 -102.36221 moveto
7.87402 -102.36221 lineto
7.87402 -86.61418 lineto
-7.87402 -86.61418 lineto
 closepath fill
newpath 7.87402 -102.36221 moveto
23.62206 -102.36221 lineto
23.62206 -86.61418 lineto
7.87402 -86.61418 lineto
 closepath fill
newpath 23.62206 -102.36221 moveto
39.37009 -102.36221 lineto
39.37009 -86.61418 lineto
23.62206 -86.61418 lineto
 closepath fill
newpath 39.37009 -102.36221 moveto
55.11812 -102.36221 lineto
55.11812 -86.61418 lineto
39.37009 -86.61418 lineto
 closepath fill
newpath 55.11812 -102.36221 moveto
70.86615 -102.36221 lineto
70.86615 -86.61418 lineto
55.11812 -86.61418 lineto
 closepath fill
newpath 70.86615 -102.36221 moveto
86.61418 -102.36221 lineto
86.61418 -86.61418 lineto
70.86615 -86.61418 lineto
 closepath fill
newpath 86.61418 -102.36221 moveto
102.36221 -102.36221 lineto
102.36221 -86.61418 lineto
86.61418 -86.61418 lineto
 closepath fill
 0.7 0.7 0.7 setrgbcolor
newpath -7.87402 -55.11812 moveto
7.87402 -55.11812 lineto
7.87402 -39.37009 lineto
-7.87402 -39.37009 lineto
 closepath fill
newpath 86.61418 -70.86615 moveto
102.36221 -70.86615 lineto
102.36221 -55.11812 lineto
86.61418 -55.11812 lineto
 closepath fill
 0.93 0.93 0.93 setrgbcolor 0.5
 0 dtransform exch truncate exch idtransform pop setlinewidth [] 0 setdash
newpath -7.87402 -102.36221 moveto
-7.87402 7.87402 lineto stroke
newpath 7.87402 -102.36221 moveto
7.87402 7.87402 lineto stroke
newpath 23.62206 -102.36221 moveto
23.62206 7.87402 lineto stroke
newpath 39.37009 -102.36221 moveto
39.37009 7.87402 lineto stroke
newpath 55.11812 -102.36221 moveto
55.11812 7.87402 lineto stroke
newpath 70.86615 -102.36221 moveto
70.86615 7.87402 lineto stroke
newpath 86.61418 -102.36221 moveto
86.61418 7.87402 lineto stroke
newpath 102.36221 -102.36221 moveto
102.36221 7.87402 lineto stroke
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath -7.87402 -102.36221 moveto
102.36221 -102.36221 lineto stroke
newpath -7.87402 -86.61418 moveto
102.36221 -86.61418 lineto stroke
newpath -7.87402 -70.86615 moveto
102.36221 -70.86615 lineto stroke
newpath -7.87402 -55.11812 moveto
102.36221 -55.11812 lineto stroke
newpath -7.87402 -39.37009 moveto
102.36221 -39.37009 lineto stroke
newpath -7.87402 -23.62206 moveto
102.36221 -23.62206 lineto stroke
newpath -7.87402 -7.87402 moveto
102.36221 -7.87402 lineto stroke
newpath -7.87402 7.87402 moveto
102.36221 7.87402 lineto stroke
 0 0 0 setrgbcolor
-5.01248 -52.34995 moveto
(S) cmmi10 14.94397 fshow
88.54173 -68.09798 moveto
(E) cmmi10 14.94397 fshow
 0 0 0.8 setrgbcolor
newpath 28.15547 -37.06372 moveto
37.06372 -28.15547 lineto
34.83665 -25.9284 lineto
25.9284 -34.83665 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 28.15547 -37.06372 moveto
37.06372 -28.15547 lineto
34.83665 -25.9284 lineto
25.9284 -34.83665 lineto
 closepath stroke
 0 0 0.8 setrgbcolor
newpath 57.42447 -28.15547 moveto
66.33272 -37.06372 lineto
68.55978 -34.83665 lineto
59.65154 -25.9284 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 57.42447 -28.15547 moveto
66.33272 -37.06372 lineto
68.55978 -34.83665 lineto
59.65154 -25.9284 lineto
 closepath stroke
 0 0 0.8 setrgbcolor
newpath 25.9284 -43.9035 moveto
34.83665 -52.81175 lineto
37.06372 -50.58469 lineto
28.15547 -41.67644 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 25.9284 -43.9035 moveto
34.83665 -52.81175 lineto
37.06372 -50.58469 lineto
28.15547 -41.67644 lineto
 closepath stroke
 0 0 0.8 setrgbcolor
newpath 59.65154 -52.81175 moveto
68.55978 -43.9035 lineto
66.33272 -41.67644 lineto
57.42447 -50.58469 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 59.65154 -52.81175 moveto
68.55978 -43.9035 lineto
66.33272 -41.67644 lineto
57.42447 -50.58469 lineto
 closepath stroke
 0 0 0.8 setrgbcolor
newpath 25.9284 -59.65154 moveto
34.83665 -68.55978 lineto
37.06372 -66.33272 lineto
28.15547 -57.42447 lineto
 closepath fill
 0 0 0 setrgbcolor
newpath 25.9284 -59.65154 moveto
34.83665 -68.55978 lineto
37.06372 -66.33272 lineto
28.15547 -57.42447 lineto
 closepath stroke
showpage
%%EOF
