#!/bin/sh
type shebang' (*' >&- 2>&-
    if [ "$1" = "--compile" ]; then
        name="${0%.ml}"
        ocamlopt -pp 'sed "1s/^#\!.*//"' \
          str.cmxa unix.cmxa "$name.ml" -o "$name" \
          || exit
        rm "$name".{cm*,o}
        exit
    else
        exec ocaml str.cma unix.cma "$0" "$@"
    fi
*)

(*
 *    A solver for the Reflexion game
 *
 * usage:
 *     reflexion-solver option* file1 option* file2 … option* filen option*
 * where ‘option’ applies to following files. If no filename is given, then
 * the standard input is processed instead.
 *
 * recognized options:
 *     ‘--sat’ or ‘--smt’
 *         specifies which backend must be used
 *     ‘--output LIST’
 *         specifies which formats to use for outputing solutions; LIST is a
 *         comma-separated list of formats among the following;
 *             — ‘ascii’: ASCII-print to standard output
 *             — ‘mp’   : generates a MetaPost source file
 *             — ‘pdf’  : generates a PDF image file
 *     ‘--’
 *         treats the following command-line argument as a filename in any case
 *
 * run directly with:
 *     ./reflexion-solver.ml ARG1 ARG2 …
 * or compile and run with:
 *     ./reflexion-solver.ml --compile
 *     ./reflexion-solver    ARG1 ARG2 …
 *
 *)


(** Utils. **)

let shell fmt = Printf.ksprintf (fun cmd -> Sys.command cmd |> ignore) fmt

let string_split = Str.split (Str.regexp " +")
let string_split_commas = Str.split (Str.regexp_string ",")

let string_set s = Bytes.set (Bytes.unsafe_of_string s)

let sscanf_from s i =
  let len = String.length s in
  let i = ref (i-1) in
  let buf = Scanf.Scanning.from_function (fun () ->
    incr i;
    if !i >= len then
      raise End_of_file
    else
      s.[!i]
  )
  in
  Scanf.bscanf buf

let rec read_lines file =
  try
    begin match input_line file with
    | ""   ->         read_lines file
    | line -> line :: read_lines file end
  with End_of_file -> []

(*
let read_file file =
  String.concat "\n" @@ read_lines file
*)
let read_file file =
  let buf = Buffer.create 256 in
  begin try while true do
    Buffer.add_string buf @@ input_line file;
    Buffer.add_char   buf '\n'
  done with End_of_file -> () end;
  Buffer.contents buf

let rec rev_init_list n f = match n with
  | 0 -> []
  | _ -> f (n-1) :: rev_init_list (n-1) f

let init_matrix h w f = Array.init h (fun i -> Array.init w (f i))

let search_matrix predicate h w m =
  let module E = struct exception Break of (int*int) end in
  try
    for i = 0 to h-1 do
      for j = 0 to w-1 do
        if predicate m.(i).(j) then
          raise @@ E.Break (i,j)
      done
    done;
    raise Not_found
  with E.Break x -> x

let search_all_matrix predicate h w m =
  let res = ref [] in
  for i = 0 to h-1 do
    for j = 0 to w-1 do
      if predicate m.(i).(j) then
        res := (i,j) :: !res
    done
  done;
  !res


(** Grid reading and writing. **)

type cell =
  | Empty
  | Diamond of int (* id *)
  | Mirror of int  (* id *)
  | Wall
  | Source
  | Exit

type direction = Up | Down | Left | Right

type game = {
  (* grid *)
  w: int;                     (* useless *)
  h: int;                     (* useless *)
  grid: cell array array;
  ascii: string array;
  (* source and exit *)
  s: int*int;
  sdir: direction;
  e: int*int;                 (* useless *)
  edir: direction;            (* useless *)
  (* mirrors *)
  mirrors: (int*int) array;
  m: int;
  (* diamonds *)
  diamonds: (int*int) array;
  d: int;
}

exception Bad_input of string

let ascii_of_mirror b = if b then '/' else '\\'

let cell_of_ascii = function
  | ' ' -> Empty
  | 'D' -> Diamond 0
  | 'M' -> Mirror 0
  | '#'
  | '%' -> Wall
  | 'S' -> Source
  | 'E' -> Exit
  | c   -> raise @@ Bad_input (Printf.sprintf "character %c" c)

let read_game file =
  (* getting the grid from the ASCII input: *)
  let ascii = read_lines file |> Array.of_list in
  let h = Array.length ascii in
  if h = 0 then
    raise @@ Bad_input "empty file";
  let w = String.length ascii.(0) in
  for i = 1 to h-1 do
    if w <> String.length ascii.(i) then
      raise @@ Bad_input "non-rectangular grid"
  done;
  let grid = init_matrix h w (fun i j -> cell_of_ascii ascii.(i).[j]) in
  (* getting the source and exit positions and directions: *)
  let s = search_matrix ((=) Source) h w grid
  and e = search_matrix ((=) Exit  ) h w grid in
  let sdir =
    begin match s with
    | (0,_) -> Down
    | (_,0) -> Right
    | (i,_) when i = h-1 -> Up
    | _                  -> Left
    end
  and edir =
    begin match e with
    | (0,_) -> Up
    | (_,0) -> Left
    | (i,_) when i = h-1 -> Down
    | _                  -> Right
    end in
  (* getting the mirrors and diamonds positions and number: *)
  let mirrors  = search_all_matrix ((=) (Mirror  0)) h w grid |> Array.of_list
  and diamonds = search_all_matrix ((=) (Diamond 0)) h w grid |> Array.of_list in
  let m = Array.length mirrors
  and d = Array.length diamonds in
  Array.iteri (fun id (i,j) -> grid.(i).(j) <- Mirror  id) mirrors;
  Array.iteri (fun id (i,j) -> grid.(i).(j) <- Diamond id) diamonds;
  { w; h; grid; ascii; s; sdir; e; edir; mirrors; m; diamonds; d }

let incr_position (i,j) = function
  | Up    -> (i-1,j)
  | Down  -> (i+1,j)
  | Left  -> (i,j-1)
  | Right -> (i,j+1)

let reflect_dir orientation = function
  | Up    -> if orientation then Right else Left
  | Down  -> if orientation then Left  else Right
  | Left  -> if orientation then Down  else Up
  | Right -> if orientation then Up    else Down

let destination game cell dir =
  let rec dest (i,j) =
    let (i',j') as cell' = incr_position (i,j) dir in
    match game.grid.(i').(j') with
    | Empty
    | Diamond _ -> dest cell'
    | Mirror id -> `Mirror (id,dir)
    | Wall
    | Source    -> `Failure
    | Exit      -> `Success
  in
  dest cell

let ray_path game orientations =
  let rec path cell dir =
    match destination game cell dir with
    | `Mirror (id,_) -> let cell' = game.mirrors.(id) in
                        cell' :: path cell' (reflect_dir orientations.(id) dir)
    | `Success       -> game.e :: []
    | `Failure       -> []
  in
  game.s :: path game.s game.sdir

let is_false_positive game orientations =
  let pos = ref game.s in
  let dir = ref game.sdir in
  let diamonds_count = ref 0 in
  let diamonds_seen = Array.make game.d false in
  while !pos <> game.e do
    pos := incr_position !pos !dir;
    let (i,j) = !pos in
    match game.grid.(i).(j) with
    | Mirror id ->
        dir := reflect_dir orientations.(id) !dir
    | Diamond id when not diamonds_seen.(id) ->
        diamonds_seen.(id) <- true;
        incr diamonds_count
    |  _ -> ()
  done;
  !diamonds_count < game.d

let output_game_as_ascii game orientations =
  Array.iteri (fun id (i,j) ->
    (*game.ascii.(i).[j] <- ascii_of_mirror orientations.(id)*)
    string_set game.ascii.(i) j (ascii_of_mirror orientations.(id))
  ) game.mirrors;
  Array.iter print_endline game.ascii

let metapost_package = {metapost_package_delimiter|
    % Reflexion macro package
    % Authors: Markus Holzer and Stefan Schwoon.

    %%%% Various definitions %%%%

    numeric unit;
    unit=5/9cm;

    color c_wall,c_grid,c_dshaded,c_mirror,c_diamond,c_path;
    c_wall=0.3white;
    c_grid=0.93white;
    c_dshaded=0.7white;
    c_mirror=0.8blue;
    c_diamond=0.7red;
    c_path=0.7green;

    unitsquare:=unitsquare shifted (-1/2*(1,1));

    path mirrorpath,diampath;
    mirrorpath=(-0.4,-0.1)--(0.4,-0.1)--(0.4,0.1)--(-0.4,0.1)--cycle;
    diampath=(0.3,0.1)--(0.1,0.3)--(-0.1,0.3)--(-0.3,0.1)--(-0.3,-0.1)--(-0.1,-0.3)--(0.1,-0.3)--(0.3,-0.1)--cycle;

    numeric last_wall,last_mirror,last_diamond;
    numeric startx, starty, targetx, targety, bpset;
    numeric wallx[],wally[],mirrorx[],mirrory[],mirrora[],diamondx[],diamondy[];
    path ballpath;
    last_wall=0;
    last_mirror=0;
    last_diamond=0;
    startx=0; starty=0;
    targetx=0; targety=0;
    bpset=0;

    %%%% Functions for defining walls, diamonds, mirrors, etc %%%%

    def wall (expr xi,yi) =
        i:=incr(last_wall);
        wallx[i]:=xi; wally[i]:=-yi;
    enddef;

    vardef start(expr x,y) = startx:=x; starty:=-y; enddef;
    vardef target(expr x,y) = targetx:=x; targety:=-y; enddef;

    def mirror(expr xi,yi,oi) =
        i:=incr(last_mirror);
        mirrorx[i]:=xi;mirrory[i]:=-yi;
        if (oi="/"): mirrora[i]:=45; fi;
        if (oi="\"): mirrora[i]:=315; fi;
    enddef;

    def diamond(expr xi,yi) =
        i:=incr(last_diamond);
        diamondx[i]:=xi; diamondy[i]:=-yi;
    enddef;

    def move_ball(expr p) =
        bpset:=1;
        ballpath:=p;
    enddef;

    %%%% Function for drawing the puzzle %%%%

    numeric xmin,ymin,xmax,ymax;

    vardef draw_grid (expr xi,yi,lx,ly) =
        for i=0 upto lx:
            draw ((xi+i-1/2,yi-1/2)--(xi+i-1/2,yi+ly-1/2)) scaled unit withcolor c_grid;
        endfor;
        for i=0 upto ly:
            draw ((xi-1/2,yi+i-1/2)--(xi+lx-1/2,yi+i-1/2)) scaled unit withcolor c_grid;
        endfor;
    enddef;

    vardef draw_all =
        beginfig(0);
        if (bpset=1):
            draw ballpath scaled unit reflectedabout ((0,0),(1,0)) dashed evenly withcolor c_path;
        fi;

        for i=1 upto last_wall:
            fill (unitsquare shifted (wallx[i],wally[i])) scaled unit withcolor c_wall;
        endfor;
        fill (unitsquare shifted (startx,starty)) scaled unit withcolor c_dshaded;
        fill (unitsquare shifted (targetx,targety)) scaled unit withcolor c_dshaded;
        xmin:=wallx[1]; xmax:=wallx[1];
        ymin:=wally[1]; ymax:=wally[1];
        for i=1 upto last_wall:
            if (wallx[i] < xmin): xmin:=wallx[i]; fi;
            if (wally[i] < ymin): ymin:=wally[i]; fi;
            if (wallx[i] > xmax): xmax:=wallx[i]; fi;
            if (wally[i] > ymax): ymax:=wally[i]; fi;
        endfor;
        draw_grid(xmin,ymin,xmax-xmin+1,ymax-ymin+1);
        label(btex $S$ etex scaled 1.5,(startx,starty) scaled unit);
        label(btex $E$ etex scaled 1.5,(targetx,targety) scaled unit);
        for i=1 upto last_mirror:
            fill mirrorpath rotated mirrora[i] shifted (mirrorx[i],mirrory[i])
                scaled unit withcolor c_mirror;
            draw mirrorpath rotated mirrora[i] shifted (mirrorx[i],mirrory[i])
                scaled unit;
        endfor;
        for i=1 upto last_diamond:
            fill diampath shifted (diamondx[i],diamondy[i])
                scaled unit withcolor c_diamond;
            draw diampath shifted (diamondx[i],diamondy[i]) scaled unit;
        endfor;

        endfig;
    enddef;
|metapost_package_delimiter}

let output_game_as_metapost file game orientations =
  (* instead of referencing an external MetaPost package, we include it directly
   * in the generated MetaPost file: *)
  (*Printf.fprintf file "input reflexion\n\n";*)
  Printf.fprintf file "%s\n\n" metapost_package;
  let pp fmt = Printf.fprintf file fmt in
  for y = 0 to game.h-1 do
    for x = 0 to game.w-1 do
      match game.grid.(y).(x) with
      | Wall      -> pp "wall(%u,%u);\n"    x y
      | Source    -> pp "start(%u,%u);\n"   x y
      | Exit      -> pp "target(%u,%u);\n"  x y
      | Diamond _ -> pp "diamond(%u,%u);\n" x y
      | Mirror id -> pp "mirror(%u,%u,\"%c\");\n" x y (ascii_of_mirror orientations.(id))
      | _ -> ()
    done
  done;
  let (y,x) = game.s in pp "\nmove_ball((%u,%u)" x y;
  List.iter (fun (y,x) -> pp "--(%u,%u)" x y) (ray_path game orientations);
  pp ");\n\ndraw_all;\nend.\n"

let output_game_as_pdf ?mp_filename pdf_filename game orientations =
  (* write the MetaPost file in the /tmp directory: *)
  let (tmp_mp_filename, tmp_mp_file) = Filename.open_temp_file "reflexion-" ".mp" in
  output_game_as_metapost tmp_mp_file game orientations;
  close_out tmp_mp_file;
  (* execute ‘mptopdf’ in the /tmp directory (as it behaves badly with files not
   * in the the current directory): *)
  let cwd = Sys.getcwd () in
  Sys.chdir (Filename.dirname tmp_mp_filename);
  shell "mptopdf %s >/dev/null" (Filename.quote (Filename.basename tmp_mp_filename));
  Sys.chdir cwd;
  (* retrieve the MetaPost file if we want to keep it: *)
  begin match mp_filename with
  | Some mp_filename ->
      (* note: the builtin ‘Sys.rename’ does not work across filesystems… *)
      shell "mv %s %s" (Filename.quote tmp_mp_filename) (Filename.quote mp_filename);
      Printf.printf "MetaPost file written to %s\n" mp_filename
  | _ -> ()
  end;
  (* retrieve the generated PDF file; *)
  let tmp_filename_prefix = Filename.chop_suffix tmp_mp_filename ".mp" in
  let tmp_pdf_filename = tmp_filename_prefix ^ "-0.pdf" in
  shell "mv %s %s" (Filename.quote tmp_pdf_filename) (Filename.quote pdf_filename);
  Printf.printf "PDF file written to %s\n" pdf_filename;
  (* clean the /tmp directory: *)
  shell "rm %s*" (Filename.quote tmp_filename_prefix)

let output_game ascii mp pdf name game orientations =
  let  mp_filename = name ^ ".mp"
  and pdf_filename = name ^ ".pdf" in
  if mp && pdf then
    output_game_as_pdf ~mp_filename pdf_filename game orientations
  else if pdf then
    output_game_as_pdf pdf_filename game orientations
  else if mp then begin
    let mp_file = open_out mp_filename in
    output_game_as_metapost mp_file game orientations;
    close_out mp_file;
    Printf.printf "MetaPost file written to %s\n" mp_filename
  end;
  if ascii then
    output_game_as_ascii game orientations


(** Genericity. **)

module type SOLVER = sig
  type literal
  type clause = literal list
  type cnf = clause list
  val literal_of_mirror: int -> bool -> literal
  val cnf_of_game: game -> cnf
  type data
  val z3_command: string
  val write_z3_input:  cnf  -> out_channel -> data
  val parse_z3_output: data -> in_channel  -> int list option
end


(** CNF generation. **)

module SAT : SOLVER = struct

  type literal =
    | False
    | True
    | M of int
    | A of int * direction
    | R of int * direction
    | Not of literal
  type clause = literal list
  type cnf    = clause  list

  let literal_of_mirror id = function
    | true -> M id
    | _    -> Not (M id)

  let neg_literal = function
    | False   -> True
    | True    -> False
    | Not lit -> lit
    | lit     -> Not lit

  let imply if_cclause then_lit =
    let dclause = List.map neg_literal if_cclause in
    then_lit :: dclause

  let clean_cnf cnf =
    cnf
    |> List.filter (fun clause -> not @@ List.mem True clause)
    |> List.map (List.filter ((<>) False))

  let dest game pos dir =
    destination game pos dir |> function
    | `Failure         -> False
    | `Success         -> True
    | `Mirror (id,dir) -> A (id,dir)

  let cnf_of_game game =
    (* the ray starts from the source cell: *)
    let initial_clause = [dest game game.s game.sdir]
    (* the ray follows the rules of reflection and moves from one mirror to another: *)
    and mirror_cnfs =
      rev_init_list game.m (fun id ->
        let mirror_dest = dest game game.mirrors.(id) in
        [
          (* traversing a mirror oriented as '/': *)
          imply [A(id,Up)   ; M id]      (R(id,Right));
          imply [A(id,Down) ; M id]      (R(id,Left));
          imply [A(id,Right); M id]      (R(id,Up));
          imply [A(id,Left) ; M id]      (R(id,Down));
          (* traversing a mirror oriented as '\': *)
          imply [A(id,Up)   ; Not(M id)] (R(id,Left));
          imply [A(id,Down) ; Not(M id)] (R(id,Right));
          imply [A(id,Right); Not(M id)] (R(id,Down));
          imply [A(id,Left) ; Not(M id)] (R(id,Up));
          (* what happens next: *)
          imply [R(id,Up)   ] (mirror_dest Up);
          imply [R(id,Down) ] (mirror_dest Down);
          imply [R(id,Left) ] (mirror_dest Left);
          imply [R(id,Right)] (mirror_dest Right);
        ]
      )
    (* every diamond is reached: *)
    and diamonds_clauses =
      rev_init_list game.d (fun id ->
        let diamond_dest = dest game game.diamonds.(id) in
        [
          diamond_dest Up;
          diamond_dest Left;
          diamond_dest Down;
          diamond_dest Right;
        ]
      )
    in
    initial_clause :: diamonds_clauses @ List.flatten mirror_cnfs
    |> clean_cnf


  (** SAT-solving using z3 with the DIMACS format. **)

  let z3_command = "z3 -dimacs -in"

  type data = literal array

  type dimacs = {
    v: int; (* number of variables *)
    c: int; (* number of clauses *)
    clauses: int list list
  }

  let dimacs_of_cnf cnf =
    let var_codes = Hashtbl.create 256
    and count = ref 0 in
    let get_code var =
      try
        Hashtbl.find var_codes var
      with Not_found ->
        incr count;
        Hashtbl.add var_codes var !count;
        !count
    in
    let clauses =
      List.map (fun clause ->
        List.map (fun lit ->
          let (sgn,var) = begin match lit with
            | Not lit -> (-1, lit)
            | lit     -> (+1, lit)
          end in
          let code = get_code var in
          sgn*code
        )
        clause
      )
      cnf
    in
    let var_mapping = Array.make (!count+1) False in
    Hashtbl.iter (fun var code -> var_mapping.(code) <- var) var_codes;
    let v = !count
    and c = List.length cnf in
    var_mapping, {v; c; clauses}

  let write_dimacs dimacs file =
    Printf.fprintf file "p cnf %u %u\n" dimacs.v dimacs.c;
    List.iter (fun clause ->
      List.iter (Printf.fprintf file "%+i ") clause;
      Printf.fprintf file "0\n"
    )
    dimacs.clauses

  let write_z3_input cnf file =
    let var_mapping, dimacs = dimacs_of_cnf cnf in
    write_dimacs dimacs file;
    var_mapping

  let read_valuation var_mapping file =
    if input_line file = "sat" then begin
      let valuation = input_line file |> string_split |> List.map (fun s ->
          let x = int_of_string s in
          let var = var_mapping.(abs x) and b = x>0 in
          (var,b)
        ) in
      Some valuation
    end
    else
      None

  let parse_z3_output var_mapping file =
    match read_valuation var_mapping file with
    | None -> None
    | Some valuation ->
        let answer =
          valuation
          |> List.map (function (M id, true) -> id | _ -> -1)
          |> List.filter ((<=) 0)
        in
        Some answer

end (* of SAT module *)


(** SMT generation. **)

module SMT : SOLVER = struct

  type _ lit =
    | M: int             -> bool lit
    | Zero:                 int  lit
    | E:                    int  lit
    | A: int * direction -> int  lit
    | R: int * direction -> int  lit
    | LT: int lit * int lit -> bool lit
    | Eq: int lit * int lit -> bool lit
    | Not: bool lit -> bool lit
  type literal = bool lit
  type clause = literal list
  type cnf    = clause  list

  let literal_of_mirror id = function
    | true -> M id
    | _    -> Not (M id)

  let neg_literal = function
    | Not lit -> lit
    | lit     -> Not lit

  let imply if_lit then_lit =
    [neg_literal if_lit; then_lit]

  let dest game pos dir =
    destination game pos dir |> function
    | `Failure         -> Zero
    | `Success         -> E
    | `Mirror (id,dir) -> A (id,dir)

  let cnf_of_game game =
    (* the ray starts from the source cell: *)
    let initial_clause = [LT (Zero, dest game game.s game.sdir)]
    (* the ray follows the rules of reflection and moves from a mirror to another: *)
    and mirror_cnfs =
      rev_init_list game.m (fun id ->
        let mirror_dest = dest game game.mirrors.(id) in
        [
          (* traversing a mirror oriented as '/': *)
          imply (M id)       (Eq (A(id,Up)   , R(id,Right)));
          imply (M id)       (Eq (A(id,Down) , R(id,Left)));
          imply (M id)       (Eq (A(id,Left) , R(id,Down)));
          imply (M id)       (Eq (A(id,Right), R(id,Up)));
          (* traversing a mirror oriented as '\': *)
          imply (Not (M id)) (Eq (A(id,Up)   , R(id,Left)));
          imply (Not (M id)) (Eq (A(id,Down) , R(id,Right)));
          imply (Not (M id)) (Eq (A(id,Left) , R(id,Up)));
          imply (Not (M id)) (Eq (A(id,Right), R(id,Down)));
          (* what happens next: *)
          imply (LT (Zero, R(id,Up)))    (LT (R(id,Up)   , mirror_dest Up));
          imply (LT (Zero, R(id,Down)))  (LT (R(id,Down) , mirror_dest Down));
          imply (LT (Zero, R(id,Left)))  (LT (R(id,Left) , mirror_dest Left));
          imply (LT (Zero, R(id,Right))) (LT (R(id,Right), mirror_dest Right));
        ]
      )
    (* every diamond is reached: *)
    and diamonds_clauses =
      rev_init_list game.d (fun id ->
        let diamond_dest = dest game game.diamonds.(id) in
        [
          LT (Zero, diamond_dest Up);
          LT (Zero, diamond_dest Left);
          LT (Zero, diamond_dest Down);
          LT (Zero, diamond_dest Right);
        ]
      )
    in
    initial_clause :: diamonds_clauses @ List.flatten mirror_cnfs


  (** SMT solving using z3 with the SMT2 format. **)

  let z3_command = "z3 -smt2 -in"

  type data = unit

  let char_of_direction = function Up -> 'u' | Down -> 'd' | Left -> 'l' | _ -> 'r'
  let direction_of_char = function 'u' -> Up | 'd' -> Down | 'l' -> Left | _ -> Right

  let rec string_of_lit :type a. a lit -> string = function
    | M id      -> Printf.sprintf "m%u" id
    | Zero        -> "z"
    | E           -> "e"
    | A (id, dir) -> Printf.sprintf "a%u%c" id (char_of_direction dir)
    | R (id, dir) -> Printf.sprintf "r%u%c" id (char_of_direction dir)
    | LT (x, y) -> Printf.sprintf "(< %s %s)" (string_of_lit x) (string_of_lit y)
    | Eq (x, y) -> Printf.sprintf "(= %s %s)" (string_of_lit x) (string_of_lit y)
    | Not lit   -> Printf.sprintf "(not %s)" (string_of_lit lit)

  let bool_variable_of_string s =
    let ss fmt res = Scanf.sscanf s fmt res in
    try
      ss "m%u" (fun id -> M id)
    with Scanf.Scan_failure _ ->
      failwith @@ "unrecognized bool variable name: " ^ s

  let int_variable_of_string s =
    let ss fmt res = Scanf.sscanf s fmt res in
    try
      ss "z" Zero
    with Scanf.Scan_failure _ -> try
      ss "e" E
    with Scanf.Scan_failure _ -> try
      ss "a%u%c" (fun id c -> A (id, direction_of_char c))
    with Scanf.Scan_failure _ -> try
      ss "r%u%c" (fun id c -> R (id, direction_of_char c))
    with Scanf.Scan_failure _ ->
      failwith @@ "unrecognized int variable name: " ^ s

  let write_z3_input cnf file =
    let var_set = Hashtbl.create 256 in
    let add var =
      if not @@ Hashtbl.mem var_set var then begin
        Hashtbl.add var_set var ();
        match var with
        | `Bool var -> Printf.fprintf file "(declare-const %s Bool)\n" (string_of_lit var)
        | `Int  var -> Printf.fprintf file "(declare-const %s Int)\n"  (string_of_lit var)
      end
    in
    let rec add_literal = function
      | LT (x, y)
      | Eq (x, y) -> add @@ `Int x; add @@ `Int y
      | Not lit   -> add_literal lit
      | lit       -> add @@ `Bool lit
    in
    List.iter (List.iter add_literal) cnf;
    let clauses = List.map (List.map string_of_lit) cnf in
    List.iter (fun clause ->
      Printf.fprintf file "(assert (or %s))\n" @@ String.concat " " clause
    )
    clauses;
    Printf.fprintf file "(check-sat)\n";
    Printf.fprintf file "(get-model)\n"

  let read_valuation () file =
    if input_line file = "sat" then begin
      let valuation =
        let s = read_file file in
        let s = String.trim s in
        let len = String.length s in
        if s.[len-1] <> ')' then
          failwith "bad format for model";
        (*s.[len-1] <- '\000';*)
        string_set s (len-1) '\000';
        let s = Scanf.sscanf s " (model %s@\000" (fun s -> s) in
        let len = String.length s in
        let rec parse i =
          if i >= len then
            []
          else
            let item, nchars = begin
              try
                sscanf_from s i " (define-fun %s () Bool %B ) %n"
                  (fun name b n -> `Bool (bool_variable_of_string name,  b), n)
              with Scanf.Scan_failure _ -> try
                sscanf_from s i " (define-fun %s () Int %i ) %n"
                  (fun name x n -> `Int  (int_variable_of_string  name,  x), n)
              with Scanf.Scan_failure _ ->
                sscanf_from s i " (define-fun %s () Int (- %i ) ) %n"
                  (fun name x n -> `Int  (int_variable_of_string  name, -x), n)
            end in
            item :: parse (i+nchars)
        in
        parse 0
      in
      Some valuation
    end
    else
      None

  let parse_z3_output () file =
    match read_valuation () file with
    | None -> None
    | Some valuation ->
        let answer =
          valuation
          |> List.map (function `Bool (M id, true) -> id | _ -> -1)
          |> List.filter ((<=) 0)
        in
        Some answer

end (* of module SMT *)



(** Main program. **)

let solve (module Solver : SOLVER) ascii mp pdf filename file =
  let solve_cnf cnf =
    let (z3_out, z3_in) as z3 = Unix.open_process Solver.z3_command in
    let data = Solver.write_z3_input cnf z3_in in
    close_out z3_in;
    let answer = Solver.parse_z3_output data z3_out in
    close_in z3_out;
    ignore @@ Unix.close_process z3;
    answer
  in
  let game = read_game file in
  let cnf = ref @@ Solver.cnf_of_game game in
  Printf.printf "%s:\n" filename;
  let again = ref true
  and tries = ref 0 in
  while !again do
    match solve_cnf !cnf with
    | None ->
        again := false;
        if !tries > 0 then
          Printf.printf "(got %u false positives)\n" !tries;
        Printf.printf "impossible\n"
    | Some mirrors ->
        let orientations = Array.make game.m false in
        List.iter (fun id -> orientations.(id) <- true) mirrors;
        if is_false_positive game orientations then begin
          incr tries;
          let forbidding_clause =
            orientations
            |> Array.mapi (fun id b -> Solver.literal_of_mirror id (not b))
            |> Array.to_list in
          cnf := forbidding_clause :: !cnf
        end
        else begin
          again := false;
          if !tries > 0 then
            Printf.printf "(got %u false positives)\n" !tries;
          output_game ascii mp pdf filename game orientations
        end
  done

(* which solver to use: *)
let sat = (module SAT : SOLVER)
and smt = (module SMT : SOLVER)
let solver = ref sat

(* which formats to output: *)
let ascii = ref true
let mp    = ref false
let pdf   = ref false

(* parsing of the command-line option ‘--output LIST’: *)
let parse_format_list list =
  ascii := false;
  mp    := false;
  pdf   := false;
  List.iter (function
    | "ascii" -> ascii := true
    | "mp"    -> mp    := true
    | "pdf"   -> pdf   := true
    | s -> raise @@ Arg.Bad ("unknown output format: " ^ s)
  )
  (string_split_commas list)

(* processing of files given on the command line; *)
let count_files_processed = ref 0
let process_file name file =
  incr count_files_processed;
  solve !solver !ascii !mp !pdf name file
let process_filename filename =
  process_file filename (open_in filename)

(* command-line options: *)
let options = Arg.(align [
  "--sat",    Unit (fun () -> solver := sat), " use the SAT solver";
  "--smt",    Unit (fun () -> solver := smt), " use the SMT solver";
  "--output", String parse_format_list, "LIST comma-separated list of output"
                                      ^ " formats, among: ascii, mp, pdf";
  "--",       String process_filename, "FILENAME treat next argument as a file name";
])

(* command-line documentation *)
let usage = Printf.sprintf
  ("usage:\n"
   ^^ "\t%s [OPTION …]               read standard input\n"
   ^^ "\t%s [OPTION …] FILENAME …    read specified files in order\n"
   ^^ "options:")
  Sys.argv.(0) Sys.argv.(0)

let () =
  Arg.parse options process_filename usage;
  if !count_files_processed = 0 then
    process_file "<stdin>" stdin
