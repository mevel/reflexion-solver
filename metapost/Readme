This package provides some support to output Reflexion puzzles graphically.

Other than this Readme, the package contains two files:

- reflexion.mp (a Metapost package; authors Markus Holzer and Stefan Schwoon)
- example.mp (an example showing how to use the package)

To output your own puzzles, you should create a file like example.mp.
The first line, "input reflexion", includes the code of reflexion.mp.
Alternatively, you can directly incorporate the contents of reflexion.mp
in your file.

The following instructions describe the contents of the cells, e.g.,
mirror(2,3) places a mirror at the cells with coordinate (2,3). Declarations
can be in any order. The presence of a start and target cell are mandatory.
The size of the puzzle is determined automatically, all non-declared cells
will be blanks. Including a trajectory for the ball is optional.

The last two lines must be "draw_all;", which generates the picture, and "end.".

Once you have created your file, say puzzle.mp, you can compile it
using "mptopdf puzzle.mp", which will create a file called "puzzle-0.pdf"
(and some additional files such as puzzle.0, puzzle.log, and reflexion.mpx).

Recommendations for use inside your solver:
- Create your files in the /tmp directory, and give them some "unique" name
  (e.g., include the process identifier, a timestamp, etc in the filename).
- Clean up all those files afterwards.

